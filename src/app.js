import $ from "jquery";
import Map from "ol/map";
import Tile from "ol/layer/tile";
import OSM from "ol/source/osm";
import View from "ol/view";
import GeoJSON from "ol/format/geojson";
import LayerVector from "ol/layer/vector";
import SourceVector from "ol/source/vector";
import Style from "ol/style/style";
import Stroke from "ol/style/stroke";
import Fill from "ol/style/fill";

import { init } from "./tree";
import { intersection } from "./spatial/intersection";

function getRandomFrom0To(num)
{
    return Math.floor(Math.random() * num);
}


$(() =>
{
    let map = new Map({
        layers: [
            new Tile({
                source: new OSM()
            }),
        ],
        target: 'map',
        view: new View({
            center: [16476860, -5274460],
            // center: [0, 0],
            zoom: 7
        })
    });
    let geoJsonFormat = new GeoJSON();

    let layersMap = {};

    init((id, layer) =>
    {
        console.log(layer);
        const color = `${getRandomFrom0To(256)}, ${getRandomFrom0To(256)}, ${getRandomFrom0To(256)}`;
        const vectorLayer = new LayerVector({
            source: new SourceVector({
                features: geoJsonFormat.readFeatures(layer, { featureProjection : 'EPSG:3857' })
            }),
            style: new Style({
                stroke: new Stroke({
                    color: `rgb(${color})`,
                    width: 1
                }),
                fill: new Fill({
                    color: `rgba(${color}, 0.1)`
                })
            })
        });
        layersMap[id] = vectorLayer;
        map.getLayers().push(vectorLayer);
    },
        id =>
    {
        if (id in layersMap)
        {
            map.getLayers().remove(layersMap[id]);
            layersMap[id] = undefined;
        }
    });

    $("#intersection").click(() =>
    {
        if ('intersection' in layersMap)
        {
            map.getLayers().remove(layersMap.intersection);
            layersMap['intersection'] = undefined;
        }
        else
        {
            const layers = map.getLayers();
            const layersLength = layers.getLength();
            let intersectionLayer = new LayerVector({
                source: new SourceVector(),
                style: new Style({
                    stroke: new Stroke({
                        color: 'green',
                        width: 1
                    }),
                    fill: new Fill({
                        color: 'rgba(0, 255, 0, 0.1)'
                    })
                })
            });

            intersection(layers.item(layersLength -1).getSource().getFeatures(),
                         layers.item(layersLength -2).getSource().getFeatures())
                .forEach(inter => intersectionLayer.getSource().addFeature(inter));
            map.getLayers().push(intersectionLayer);
        }
    });
});
