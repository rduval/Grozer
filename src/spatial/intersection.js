import GeoJSON from "ol/format/geojson";
import extent from "ol/extent";
import * as turf from "@turf/turf";

export function intersection(features1, features2)
{
    const geoJsonFormat = new GeoJSON();

    let result = [];
    features1.forEach(feature1 =>
    {
        features2.forEach(feature2 =>
        {
            if (!extent.intersects(feature2.getGeometry().getExtent(),
                            feature1.getGeometry().getExtent()))
            {
                return;
            }
            const geoJson1 = geoJsonFormat.writeFeatureObject(feature1);
            const geoJson2 = geoJsonFormat.writeFeatureObject(feature2);

            const geometries1 = extractGeometry(geoJson1);
            const geometries2 = extractGeometry(geoJson2);

            geometries1.forEach(geom1 =>
            {
                geometries2.forEach(geom2 =>
                {
                    const intersection = turf.intersect(geom1, geom2);
                    if (intersection)
                    {
                        result.push(geoJsonFormat.readFeature(intersection));
                    }
                });
            });

        });
    });
    return result;
}

function extractGeometry(geoJson)
{
    if (geoJson.geometry.type === "MultiPolygon")
        return geoJson.geometry.coordinates.map(coord =>
        {
            let geom = { type: "Polygon" };
            geom.coordinates = coord;
            return geom;
        });
    else
        return [geoJson.geometry];
}
