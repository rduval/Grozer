import $ from 'jquery';
import { ThemeFetcher } from "./layers/ThemeFetcher";

var callbacks = {
    onShowLayer: undefined,
    onHideLayer: undefined
};
var layers = [];

export function init(onShowLayer, onHideLayer)
{
    callbacks.onShowLayer = onShowLayer;
    callbacks.onHideLayer = onHideLayer;

    let themes = new ThemeFetcher();
    themes.getWrkspaces().then(fetcher =>
    {
        Object.keys(fetcher.layerHierarchy).forEach(wrkSpace =>
        {
            $("#tree").append(`<li><p class="wrkspace" id="${wrkSpace}">${wrkSpace}</p></li>`);
        });
        $(".wrkspace").click(get_themes(fetcher));
    });
}


function get_themes(fetcher)
{
    return event =>
    {
        const wrkspace = event.target.id;

        if ($(`p#${wrkspace}`).parent().children().length === 1)
        {
            fetcher.getThemes(wrkspace).then(fetcher =>
            {
                $(`p#${wrkspace}`).parent().append(`<ul id="${wrkspace}"></ul>`);
                Object.keys(fetcher.layerHierarchy[wrkspace]).forEach(theme =>
                {
                    $(`ul#${wrkspace}`).append(`<li><p class="theme" id="${wrkspace}-${theme}">${theme}</p></li>`);
                });
                $(".theme").click(get_layers(fetcher));
            });
        }
    };
}

function get_layers(fetcher)
{
    return event =>
    {
        const wrkspaceTheme = event.target.id;
        if ($(`p#${wrkspaceTheme}`).parent().children().length == 1)
        {
            const [wrkspace, theme] = event.target.id.split('-');
            fetcher.getLayers(wrkspace, theme).then(fetcher =>
            {
                $(`p#${wrkspaceTheme}`).parent().append(`<ul id="${wrkspaceTheme}"></ul>`);
                Object.keys(fetcher.layerHierarchy[wrkspace][theme]).forEach(layer =>
                {
                    $(`ul#${wrkspaceTheme}`).append(`<li><p class="layer" id="${wrkspaceTheme}-${layer}">${layer}</p></li>`);
                });
                $(".layer").click(get_layer(fetcher));
            });
        }
    };
}

function get_layer(fetcher)
{
    return event =>
    {
        const triggerElement = $(event.target);
        if (triggerElement.data("item") === undefined || triggerElement.data("item") === -1)
        {
            console.log(event.target.id);
            const [wrkspace, theme, layer] = event.target.id.split('-');

            let handleShow = (fetcher) =>
            {
                const layerData = fetcher.layerHierarchy[wrkspace][theme][layer];
                triggerElement.data("item", layers.push(layerData) -1);
                callbacks.onShowLayer(triggerElement.data("item"), layerData);
            };

            if ($.isEmptyObject(fetcher.layerHierarchy[wrkspace][theme][layer]))
            {
                fetcher.getLayer(wrkspace, theme, layer).then(fetcher =>
                {
                    handleShow(fetcher);
                });
            }
            else
            {
                handleShow(fetcher);
            }
        }
        else
        {
            const index = triggerElement.data("item");
            console.log(index);
            let tmp = layers.slice(0, index);
            layers.slice(index +1).forEach(elem => tmp.push(elem));
            layers = tmp;
            callbacks.onHideLayer(index);
            triggerElement.data("item", -1);
        }
    };
}
